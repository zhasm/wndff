# settings.py
consumer_key = ''   # api key
consumer_secret = ''  # api secret
oauth_token = ''
oauth_token_secret = ''
blacklist=["",]
search_keywords=[]
SLP=59
LOG_INTERVAL=10

try:
    from local_settings import *
except:
    pass
