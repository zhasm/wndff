#!/usr/bin/env   python
# -*- encoding:  utf-8 -*-
#
#       Author:  Rex Zhang
#  Create Time:  2014-02-01 19:04
#    File name:  git/wndff/msgqueue.py
import memcache


class MsgQueue():

    def __init__(self, name='wndff'):
        self._mc = memcache.Client(['127.0.0.1:11211'], debug=0)
        self._prefix = name

    def key(self):
        return '%s:msgs' % self._prefix

    def get(self):
        key = self.key()
        msgs = self._mc.get(key)
        if msgs and len(msgs):
            msg = msgs.pop()
            self.set(msgs)
            return msg

    def set(self, msgs):
        key = self.key()
        self._mc.set(key, msgs)

    def push(self, msgs):
        key = self.key()
        _msgs = self._mc.get(key)

        for m in msgs:
            _msgs.insert(0, m)

        self.set(_msgs)

    def length(self):
        key = self.key()
        _msgs = self._mc.get(key) or []
        return len(_msgs)
