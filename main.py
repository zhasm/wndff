#!/usr/bin/env   python
# -*- encoding:  utf-8 -*-
#
#       Author:  Rex Zhang
#  Create Time:  2014-02-01 16:44
#    File name:  git/wndff/main.py

from msgqueue import MsgQueue
from producer import producer
from time import sleep
from time import sleep, strftime
from wannengdefanfou import Wndff, forceUTF8
import os

def getArgs():
    """show argpase snippets"""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sleep', help='sleep seconds. ', default=60, type=int)
    parser.add_argument('-p', '--pidfile', help='pid file name')

    return parser.parse_args()

def callback(body):
    try:
        #msg = json.loads(body)
        msg = body
        ff = Wndff()
        ff.post(msg)

    except Exception as e:
        print 'exception when posting: %s' % str(e)

forceUTF8()
pid = os.getpid()
mq = MsgQueue()

def log(msg):
    print strftime('[%Y-%m-%d %X]') + ('[p:%s] ' % pid) +  str(msg)

def main():
    args = getArgs()

    ITV = args.sleep or 60
    pidfile = args.pidfile

    if pid and pidfile:
        with open(pidfile, 'w') as f:
            f.write('%s' % pid)

    ff = Wndff().ff

    while True:

        msgs = ff.search()
        rpls = ff.rpl_search()

        if msgs:
            producer(msgs)

        if rpls:
            producer(rpls)

        log('pub_tl: %d; rpl: %d; mq: %d; api_limit: %d' % (
            len(msgs),
            len(rpls),
            mq.length(),
            ff.limit(),
        ))

        msg = mq.get()
        slp_counter = 0
        while msg:
            callback(msg)
            log('[-1]')
            msg = mq.get()
            sleep(1)
            slp_counter += 1

        sleep(ITV - slp_counter)

if __name__ == '__main__':
    main()
