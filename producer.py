#!/usr/bin/env   python
# -*- encoding:  utf-8 -*-
#
#       Author:  Rex Zhang
#  Create Time:  2014-02-01 20:05
#    File name:  git/wndff/producer.py

from msgqueue import MsgQueue
from wannengdefanfou import forceUTF8

def producer(msg):
    mq = MsgQueue()
    mq.set(msg)
