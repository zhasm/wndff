#!/usr/bin/env python
# -*- coding: utf-8 -*-

# code by from1to9
# from1to9@gmail.com

import oauth2 as oauth
import re, time#
import codecs


class Api:
	api_base = "api.fanfou.com"
#extention="xml" #json, xml, rss
	extension = 'json'

	api = {
		"public_timeline": {"url": "statuses/public_timeline", "method": "GET"},
		"friends_timeline": {"url": "statuses/friends_timeline", "method": "GET"},
		"replies": {"url": "statuses/replies", "method": "GET"},
		"mentions": {"url": "statuses/mentions", "method": "GET"},
		"show": {"url": "statuses/show", "method": "GET"},
		"user_timeline": {"url": "statuses/user_timeline", "method": "GET"},
		"update": {"url": "statuses/update", "method": "POST"},
		"destroy": {"url": "statuses/destroy", "method": "POST"},
		"statuses_friends": {"url": "statuses/friends", "method": "POST"},
		"statuses_followers": {"url": "statuses/followers", "method": "POST"},
	#photo
		"photo_upload": {"url": "photos/upload", "method": "POST"},
	#private msg
		"direct_messages_sent": {"url": "direct_messages/sent", "method": "GET"},
		"direct_messages_inbox": {"url": "direct_messages/inbox", "method": "GET"},
		"direct_messages_new": {"url": "direct_messages/new", "method": "POST"},
		"direct_messages_destroy": {"url": "direct_messages/destroy", "method": "POST"},
	#friends
		"friendships_create": {"url": "friendships/create", "method": "POST"},
		"friendships_destroy": {"url": "friendships/destroy", "method": "POST"},
		"friendships_exists": {"url": "friendships/exists", "method": "GET"},
		"friends_ids": {"url": "friends/ids", "method": "GET"},
		"followers_ids": {"url": "followers/ids", "method": "GET"},
	#user
		"users_show": {"url": "users/show", "method": "GET"},
		"users_followers": {"url": "users/followers", "method": "GET"},
		"users_friends": {"url": "users/friends", "method": "GET"},
	#notification
		"notifications_follow": {"url": "notifications/follow", "method": "POST"},
		"notifications_leave": {"url": "notifications/leave", "method": "POST"},
        #search
		"search": {"url": "search/public_timeline", "method": "GET"},
	#misc
		"limit": {"url": "account/rate_limit_status", "method": "GET"},
	}


	def __init__(self, api_type):
		self.api_type = api_type

	def url(self):
		if self.api_type in self.api.keys():
			api = self.api[self.api_type]
			url = "http://" + self.api_base + "/" + api["url"] + "." + self.extension
			return url
		else:
			return ""

	def method(self):
		return self.api[self.api_type]["method"]

class fanfou:

	def __init__( self, consumer_key, consumer_secret, oauth_token, oauth_token_secret ):
		self.__token = oauth.Token(key = oauth_token, secret = oauth_token_secret)
		self.__consumer = oauth.Consumer(key = consumer_key, secret = consumer_secret)
		self.__client = oauth.Client(self.__consumer, self.__token)

	def _execute( self, params = {}, method = "GET", url = "" ):
		try:
			if method == "POST":
				req = oauth.Request(method = method, url = url, parameters = params, is_form_encoded = True)
				resp, content = self.__client.request(uri = url, method = "POST", body = req.to_postdata())
			else:
				req = oauth.Request(method = method, url = url, parameters = params)
				resp, content = self.__client.request(uri = req.to_url(), method = "POST")
			rev = {"resp": resp, "content": content}
			return rev
		except Exception, e:
			print str(e);pass

	def call( self , apitype, params ):
		api = Api(apitype)

		rev = self._execute( params, api.method(), api.url() )
		return rev

	def update( self, message, source = "", in_reply_to_status_id=""):
		params = {
			"status": message,
			"source": source,
			"in_reply_to_status_id": "",
			#"in_reply_to_user_id": "",
			#"repost_status_id": "",
			#"location": "",
		}
		rev = self.call("update", params)
		return rev

	def reply_to( self, message, in_reply_to_status_id, in_reply_to_user_id, source = "" ):
		params = {
			"status": message,
			"source": source,
			"in_reply_to_status_id": in_reply_to_status_id,
			"in_reply_to_user_id": in_reply_to_user_id,
		}
		rev = self.call("update", params)
		return rev

	def repost( self, message, repost_status_id, source = "" ):
		params = {
			"status": message,
			"source": source,
			"repost_status_id": repost_status_id,
		}
		rev = self.call("update", params)
		return rev

	def photo(self, photo, message = "", source = ""):
		params = {
			"photo": photo,
			"status": message,
			"source": source,
		}
		#rev = self.call("photo_upload", params)
		#return rev

	def replies(self, since_id = "", mode = "lite", format = ""):
		params = {
			"since_id": since_id,
			#"count": count,
			#"page": page,
			"mode": mode,
			"format": format,
		}
		rev = self.call("replies", params)
		return rev

	def mentions(self, since_id = "", mode = "lite", format = ""):
		params = {
			"since_id": since_id,
			#"count": count,
			#"page": page,
			"mode": mode,
			"format": format,
		}
		rev = self.call("mentions", params)
		return rev

	def direct_messages_inbox(self, since_id = "", mode = "lite"):
		params = {
			"since_id": since_id,
			#"count": count,
			#"page": page,
			"mode": mode,
		}
		rev = self.call("direct_messages_inbox", params)
		return rev

	def direct_messages_new(self, user, text, in_reply_to_id = ""):
		params = {
			"user": user,
			"text": text[:140],
			"in_reply_to_id": in_reply_to_id,
			"mode": "lite",
			"format": format,
		}
		rev = self.call("direct_messages_new", params)
		return rev

	def friends_ids(self, id = "", page = "1", count = "60"):
		params = {
			"id": id,
			"count": count,
			"page": page,
			#"mode": mode,
		}
		rev = self.call("friends_ids", params)
		return rev

	def followers_ids(self, id = "", page = "1", count = "60"):
		params = {
			"id": id,
			"count": count,
			"page": page,
			#"mode": mode,
		}
		rev = self.call("followers_ids", params)
		return rev

	def users_show(self, id = ""):
		params = {
			"id": id,
			"mode": "lite",
			#"mode": mode,
		}
		rev = self.call("users_show", params)
		return rev

	def search(self, q, since_id="", count=50, mode='lite'):
		params = {
			"q": q,
			"since_id": since_id,
			"count": count,
			"mode": mode,
			}

		rev = self.call("search", params)
		return rev

	def limit(self):
		params = {}
		rev = self.call("limit", params)
		return rev

if __name__ == "__main__":
	consumer_key = ''   # api key
	consumer_secret = ''  # api secret
	oauth_token = ''
	oauth_token_secret = ''

	fanfou = fanfou(consumer_key, consumer_secret, oauth_token, oauth_token_secret)
	msg = fanfou.update("test pyfanfou")
	print msg["content"].decode("utf-8")
	#raw_input()
