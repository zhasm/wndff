def unescape(s):
    s=s.replace("&lt;","<")
    s=s.replace("&gt;",">")
    s=s.replace("&quot;",'"')
    s=s.replace("&apos;","'")
    s=s.replace("&amp;","&")
    s=s.replace("\\","")
    return s

from os.path import dirname as dirname
from os.path import join as pathjoin

def getPath(sufix=""):
    '''get absolute path of the current dir'''
    path = dirname(__file__)
    try:
        index=path.index("..")
        if index!=-1:
            path=path[:index]
    except:
        pass
    return pathjoin(path, sufix).replace('\\','/')
