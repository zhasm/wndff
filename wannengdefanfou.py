#!/usr/bin/env python
# -*- coding: utf-8 -*-

from fanfou import fanfou
from logger import log, err
from misc import getPath
from misc import unescape
from settings import bad_words, blacklist, consumer_key, consumer_secret, oauth_token, oauth_token_secret, search_keywords
from time import sleep
import json
import re
import sys
from pprint import pprint
sys.path.append("~/Dropbox/tips")
from snippet import unescape, forceUTF8
import memcache


#from facedetect import FaceDetect

def msg_filter(msgs, filter_at=True):

    def _has_bad_word(content):

        content = re.sub(r'&lt;/?strong&gt;', '', content).replace(u'@万能的饭否', '').replace('@万能的饭否', '')

        if filter_at and '@' in content:
            return True

        for b in bad_words:
            if b in content:
                return True

        return False

    msgs = [m for m in msgs if not m.get('repost_status_id')
              and m.get('user', {}).get('id', '') not in blacklist
              and not _has_bad_word(m.get('text'))
    ]

    return msgs


class LastSinceID():

    def __init__(self, key_prefix='wndff'):
        self.mc = memcache.Client(['127.0.0.1:11211'], debug=0)
        self.key = '%s:lastSinceID' % key_prefix

    def get(self):
        return self.mc.get(self.key) or ''

    def set(self, value):
        if value:
            ret = self.mc.set(self.key, value)
            return ret


class FF:

    def __init__(self, consumer_key, consumer_secret, oauth_token, oauth_token_secret, key):
        self.ff=fanfou(consumer_key, consumer_secret, oauth_token, oauth_token_secret)
        self.key_manager = LastSinceID()
        self.reply_key_manager = LastSinceID('wndff_rpl')
        self.key = key

    def update(self, msg, repost_status_id=''):
        msg=unescape(msg)
        return self.ff.repost(msg, repost_status_id)

    def getMaxSinceID(self, msgs):
        if msgs:
            m=msgs[0]
            try:
                return m.get('id')
            except Exception as e:
                err("error getmaxsinceid: "+ str(e))
                return ""
        return ""

    def search(self):
        since_id = self.key_manager.get() or ''
        q = self.key

        if not q:
            err("Empty query word!")
            return []
        try:
            msgs = self.ff.search(q, since_id)['content']
            msgs = json.loads(msgs)
            id = self.getMaxSinceID(msgs)
            self.key_manager.set(id)
            if since_id:
                return msg_filter(msgs)
            else:
                return []

        except Exception as e:
            print 'search error', e
            return []

    def rpl_search(self):
        since_id = self.reply_key_manager.get() or ''

        try:
            msgs = self.ff.replies(since_id)['content']
            msgs = json.loads(msgs)
            if not msgs:
                return []
            id = self.getMaxSinceID(msgs)

            self.reply_key_manager.set(id)

            if since_id:
                return msg_filter(msgs, filter_at=False)
            else:
                return []

        except Exception as e:
            print 'search error', e
            return []


    def limit(self):
        l=self.ff.limit()
        return json.loads(l['content'])['remaining_hits']

def ue(s):
    'unescape'
    s=unescape(unescape(s))
    s=re.sub(r"""<[^<>]+>""", "", s)
    return s


class Wndff():

    def __init__(self, key=search_keywords):
        self.ff= FF(consumer_key, consumer_secret, oauth_token, oauth_token_secret, key)

    def post(self, m):
        realname=m['user']['screen_name']
        content=ue(m['text'])
        msgid=m['id']

        userid=m['user']['id']

        msg="转@%(name)s %(msg)s" % {
                "name": realname,
                "msg":  content,
        }

        msg = re.sub(r'\s*@(?:%s|%s)\s*' % ('万能的饭否', u'万能的饭否', ), ' ', msg)
        print '[wd]Msg: %s' % msg

        self.ff.update(msg, msgid)
