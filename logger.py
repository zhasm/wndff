import logging
import time
# create logger
import os
LOG_FILENAME="log"

logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
#logger = logging.getLogger("Sync")
logger = logging.getLogger("")
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

logger.addHandler(ch)

os.environ['TZ'] = 'Asia/Shanghai'
time.tzset()

def log(message):
    message=str(message)
    logger.info(time.strftime('[%Y-%m-%d %X] ')+message)

def err(message):
    message=str(message)
    logger.error(time.strftime('[%Y-%m-%d %X] ')+message)


